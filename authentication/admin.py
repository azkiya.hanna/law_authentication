from django.contrib import admin
from .models import ClientModel, UserModel
# Register your models here.
admin.site.register(ClientModel)
admin.site.register(UserModel)