from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from .models import ClientModel, UserModel
from django.utils import timezone
from datetime import datetime
import hashlib
import random

# Create your views here.
@csrf_exempt
def login(request):
    if request.method == 'POST':
        if ("grant_type", "password") in request.POST.items():
            user_id = request.POST.get('username')
            password = request.POST.get('password')
            encodedPassword = hashlib.sha256(password.encode()).hexdigest()
            client_id = request.POST.get('client_id')
            client_secret = request.POST.get('client_secret')
        try:
            # mendapatkan akun client
            client_account = ClientModel.objects.get(client_id=client_id,client_secret=client_secret)
            # mendapatkan akun user yang terdaftar dengan akun client diatas 
            user_account = UserModel.objects.get(client=client_account,user_id=user_id,password=encodedPassword)
        except:
            return JsonResponse({'error':'invalid_request','error_description':'ada kesalahan masbro!'},status=401)
        
        # membuat access token
        dateTimeObj = datetime.now()
        access_token_string = client_id + user_id + dateTimeObj.strftime("%d-%b-%Y:%H:%M:%S.%f)")
        access_token = hashlib.sha1(access_token_string.encode()).hexdigest()
        # membuat refresh token
        randomInt = random.randint(0,9) + 1000
        refresh_token_string = access_token + str(randomInt)
        refresh_token = hashlib.sha1(refresh_token_string.encode()).hexdigest()
        # update access & refresh token dan last_logged_in akun user
        user_account.access_token = access_token
        user_account.refresh_token = refresh_token
        user_account.last_logged_in = timezone.now()
        user_account.save()

        # mengembalikan response 
        responseData = {
            'access_token': access_token,
            'expires_in': 300,
            'token_type' : "Bearer",
            'scope' :  None,
            'refresh_token' : refresh_token
        }
        return JsonResponse(responseData,status=200)    
    if request.method == 'GET':
        return JsonResponse({'username': 'tami99','password': 'tamihoag1999', 'grant_type': 'password','client_id':'a2d9baea-1dec-482d-8377-6c5c5f01bebc','client_secret':'53686f70656532303232'},status=200)

@csrf_exempt
def get_resource(request):
    if request.method == 'POST':
        authorization = request.headers.get('Authorization')
        try:
            access_token = authorization.split()[1]
            user_account = UserModel.objects.get(access_token=access_token)
            expired_time = user_account.last_logged_in + timezone.timedelta(seconds=300)
            if timezone.now() < expired_time:
                responseData = {
                    'access_token': user_account.access_token,
                    'client_id': user_account.client.client_id,
                    'user_id' : user_account.user_id,
                    'full_name' : user_account.full_name,
                    'npm' : user_account.npm,
                    'expires': None,
                    'refresh_token' : user_account.refresh_token
                }
                return JsonResponse(responseData,status=200)
            else:
                return JsonResponse({'error':'invalid_token','error_description':'Token Salah masbro'},status=401)
        except:
            return JsonResponse({'error':'invalid_token','error_description':'Token Salah masbro'},status=401)
        


    
