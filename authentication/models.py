from django.db import models
from django.utils import timezone

# Create your models here.
class ClientModel(models.Model):
    client_id = models.UUIDField()
    client_secret = models.CharField(max_length=32)

class UserModel(models.Model):
    access_token = models.CharField(max_length=40)
    user_id = models.CharField(max_length=15)
    password = models.CharField(max_length=64)
    full_name = models.CharField(max_length=30)
    npm = models.CharField(max_length=10)
    refresh_token = models.CharField(max_length=40)
    client = models.ForeignKey(ClientModel, on_delete=models.CASCADE)
    last_logged_in = models.DateTimeField(default=timezone.now)
