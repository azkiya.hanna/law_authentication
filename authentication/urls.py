from django.urls import path
from . import views

app_name = 'authentication'

urlpatterns = [
    path('oauth/token', views.login, name='index'),
    path('oauth/resource', views.get_resource, name='resource'),
]